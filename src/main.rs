use image::io::Reader as ImageReader;
use std::error::Error;
use itertools::Itertools;
use std::env;

// I think that my rust_analyzer is broken. I still am struggling to read the docs and know what
// piece I am working with, without using the let _: () = trick...


struct RGBA {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
}

// This is not technically a "standard" but I've seen it work for most terminals
fn to_ansi_rgb(rgb: &RGBA, output: &mut String) {
    if rgb.a == 0 {
        output.push_str("  ");
    }
    else {
        output.push_str(
            format!("\x1b[48;2;{};{};{}m  \x1b[0m",
                    &rgb.r, &rgb.g, &rgb.b).as_str());
    }
}

fn convert_img(img_path: &str) -> Result<String, Box<dyn Error>> {
    let input_image = ImageReader::open(img_path)?.decode()?;
    let mut output: String = String::new();

    // IDK how to make this easier to read into chunks that match the row...
    for row in &input_image.as_bytes().into_iter().chunks((input_image.width() * 4) as usize) {
        // let r = row.i ;
        let r: Vec<&u8> = row.into_iter().collect();
        for pixel in r.chunks(4) {
            let rgb = RGBA {
                r: pixel[0].clone(),
                g: pixel[1].clone(),
                b: pixel[2].clone(),
                a: pixel[3].clone(),
            };

            to_ansi_rgb(&rgb, &mut output);
        }
        output.push_str("\n");
    }
    Ok(output.replace("\\", "\\\\"))
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("You should give me a path to an image, ideally one with an alpha channel.\n something like::\n\tcolour_feller /path/to/img.png");
    }
    else {
        println!("{}", convert_img(args[1].as_str()).unwrap());
    }
    Ok(())
}

