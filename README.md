# Colour Feller

I like ANSI art stuff, This is a thing that I do a lot, so I tend to make a thing to convert png images to their ANSI equivilant pretty regularly.

## Installation
I took the liberty of taking the build from this and putting it as a binary in the gitlab releases. Use at your own risk. I hardly know what I am doing, and don't make any guarantee that won't get you popped. ರ╭╮ರ

[You can get the binary (only x86 for now) here](https://gitlab.com/ShiddyProgramming/colour_feller/-/artifacts) under the public directory.

## Building

```
git clone https://gitlab.com/ShiddyProgramming/colour_feller.git
cd colour_feller
cargo build --release

# Move this puppy to your PATH somewhere
cp ./target/release/colour_feller /home/user/.local/bin/
```

## Usage
convert an image!

```
colour_feller ./kirbo.png
```

**NOTE** I only tested this with PNG images with alpha channels, if you omit the alpha channel it will not select each pixel appropriately.
